package com.example.preexamenunidad1;

public class ReciboNomina {
    private int numRecibo;
    private String nombre;
    private float horasTrabNormal;
    private float horasTrabExtras;
    private int puesto; // 1: auxiliar, 2: albañil, 3: ing obra
    private float impuestoPorc;

    public ReciboNomina(int numRecibo, String nombre, float horasTrabNormal, float horasTrabExtras, int puesto, float impuestoPorc) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.puesto = puesto;
        this.impuestoPorc = impuestoPorc;
    }

    public float calcularSubtotal() {
        float pagoBase = 200;
        if (puesto == 1) {
            pagoBase += pagoBase * 0.2;
        } else if (puesto == 2) {
            pagoBase += pagoBase * 0.5;
        } else if (puesto == 3) {
            pagoBase += pagoBase * 1.0;
        }
        return (horasTrabNormal * pagoBase) + (horasTrabExtras * pagoBase * 2);
    }

    public float calcularImpuesto(float subtotal) {
        return subtotal * (impuestoPorc / 100);
    }

    public float calcularTotal(float subtotal, float impuesto) {
        return subtotal - impuesto;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float getImpuestoPorc() {
        return impuestoPorc;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }
// Getters y setters
}

