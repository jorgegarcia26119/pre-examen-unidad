package com.example.preexamenunidad1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class ReciboNominaActivity extends AppCompatActivity {

    private TextView lblNombreRecibo;
    private EditText txtHorasTrabajadas, txtHorasExtras;
    private RadioGroup rgPuesto;
    private RadioButton rbAuxiliar, rbAlbanil, rbIngObra;
    private Button btnCalcular, btnLimpiar, btnRegresar;
    private TextView lblSubtotal, lblImpuesto, lblTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);

        lblNombreRecibo = findViewById(R.id.lblNombreRecibo);
        txtHorasTrabajadas = findViewById(R.id.txtHorasTrabajadas);
        txtHorasExtras = findViewById(R.id.txtHorasExtras);
        rgPuesto = findViewById(R.id.rgPuesto);
        rbAuxiliar = findViewById(R.id.rbAuxiliar);
        rbAlbanil = findViewById(R.id.rbAlbanil);
        rbIngObra = findViewById(R.id.rbIngObra);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        lblSubtotal = findViewById(R.id.lblSubtotal);
        lblImpuesto = findViewById(R.id.lblImpuesto);
        lblTotal = findViewById(R.id.lblTotal);

        String nombre = getIntent().getStringExtra("nombre");
        lblNombreRecibo.setText("Nombre: " + nombre);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularNomina(nombre);
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresarMenu();
            }
        });
    }

    private void calcularNomina(String nombre) {
        float horasTrabajadas = Float.parseFloat(txtHorasTrabajadas.getText().toString());
        float horasExtras = Float.parseFloat(txtHorasExtras.getText().toString());
        int puesto = 0;
        if (rbAuxiliar.isChecked()) {
            puesto = 1;
        } else if (rbAlbanil.isChecked()) {
            puesto = 2;
        } else if (rbIngObra.isChecked()) {
            puesto = 3;
        }

        ReciboNomina reciboNomina = new ReciboNomina(1, nombre, horasTrabajadas, horasExtras, puesto, 16);
        float subtotal = reciboNomina.calcularSubtotal();
        float impuesto = reciboNomina.calcularImpuesto(subtotal);
        float total = reciboNomina.calcularTotal(subtotal, impuesto);

        lblSubtotal.setText("Subtotal: " + subtotal);
        lblImpuesto.setText("Impuesto: " + impuesto);
        lblTotal.setText("Total a pagar: " + total);
    }

    private void limpiarCampos() {
        txtHorasTrabajadas.setText("");
        txtHorasExtras.setText("");
        rgPuesto.clearCheck();
        lblSubtotal.setText("Subtotal: ");
        lblImpuesto.setText("Impuesto: ");
        lblTotal.setText("Total a pagar: ");
    }

    private void regresarMenu() {
        Intent intent = new Intent(ReciboNominaActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}


